import os
import cv2
import multiprocessing as mp
from src import metrics as qpar
import pandas as pd
from time import time
import argparse
import sys
def get_params(filename):
    snr = []
    b = []
    c = []
    l = []
    brisque = []
    try:
        snr.append(qpar.snr(filename))
        b.append(qpar.laplacian(filename))
        c.append(qpar.michelson(filename))
        l.append(qpar.brightness(filename))
        brisque.append(qpar.bris(filename))
    except NameError:
        print(NameError)
        pass

    return snr, b, c, l, brisque

def get_filenames(base):
    X0 = []
    Y0 = []
    ext = ('jpg', 'jpeg', 'png', 'tif', 'PNG', 'JPG','JPEG')
    for i in sorted(os.listdir(base)):
        if i.endswith(ext):
            X0.append(os.path.join(base, i))
            Y0.append(i)
    return X0,Y0

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', help='Source image path', required=True)
    parser.add_argument('-c', help='CSV file name', required=True)
    args = parser.parse_args()
    base = args.s
    csv_write = args.c + '.csv'
    pool = mp.Pool(processes=mp.cpu_count())
    start = time()
    X0, Y0 = get_filenames(base)
    # for i in X0:
    #     SNR, B, C, L, BRIS = get_params(i)
    #     print(SNR, B, C, L, BRIS)
    # sys.exit()
    SNR, B, C, L, BRIS = zip(*pool.map(get_params, X0))
    snr = map(lambda x: x[0], SNR)
    b = map(lambda x: x[0], B)
    c = map(lambda x: x[0], C)
    l = map(lambda x: x[0], L)
    bris = map(lambda x: x[0], BRIS)
    df1 = pd.DataFrame({'File': Y0,
                        'SNR': snr,
                        'Blur': b,
                        'Contrast': c,
                        'Brightness': l,
                        'Brisque': bris,
                        })
    df1.to_csv(csv_write)
    pool.close()
    pool.join()
    stop = time()
    et = stop - start
    print('[Elapsed time]: '+ str(et) + ' s')

if __name__ == '__main__':
    main()


