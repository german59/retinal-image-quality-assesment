import pandas as pd
import numpy as np
from time import time
import argparse
from sklearn.cluster import KMeans
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def KMeans_predict(CSV_read,CSV_write):
    csv = pd.read_csv(CSV_read)
    FILE = csv['File']
    SNR = csv['SNR']
    BLUR = csv['Blur']
    CONT_RMS = csv['Contrast']
    file = FILE.to_list()

    snr = SNR.to_numpy(float)
    blur = BLUR.to_numpy(float)
    cont = CONT_RMS.to_numpy(float)

    x = np.array([snr, blur, cont])
    data = x.transpose((1,0))

    kmeans = KMeans(n_clusters=2).fit(data)
    centroids = kmeans.cluster_centers_
    print(centroids)
    labels = kmeans.predict(data)
    print(type(labels))
    output_kmeans = labels.tolist()
    print(type(output_kmeans))
    # Getting the cluster centers
    C = kmeans.cluster_centers_
    colores = ['red', 'blue']
    asignar = []
    for row in labels:
        asignar.append(colores[row])
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.scatter(data[:, 0], data[:, 1], data[:, 2], c=asignar, s=1)
    ax.scatter(C[:, 0], C[:, 1], C[:, 2], marker='*', c=colores, s=1)
    plt.show()
    filename = []
    snr1 = []
    blur1 = []
    cont1 = []

    for snr0, blur0, CRMS0, file0 in zip(SNR, BLUR, CONT_RMS, FILE):
            filename.append(file0)
            snr1.append(snr0)
            blur1.append(blur0)
            cont1.append(CRMS0)
    df1 = pd.DataFrame({'File': filename,
                        'SNR': snr1,
                        'Blur': blur1,
                        'Contrast': cont1,
                        'KM_label': output_kmeans}
                       )
    df1.to_csv(CSV_write+'.csv')
def get_cluster_points(CSV_read):
    csv = pd.read_csv(CSV_read)
    SNR = csv['SNR']
    BLUR = csv['Blur']
    CONT_RMS = csv['Contrast']

    snr = SNR.to_numpy(float)
    blur = BLUR.to_numpy(float)
    cont = CONT_RMS.to_numpy(float)

    csnr  = np.sort(np.squeeze(KMeans(n_clusters=3).fit(snr.reshape(-1, 1)).cluster_centers_))
    cblur = np.sort(np.squeeze(KMeans(n_clusters=3).fit(blur.reshape(-1, 1)).cluster_centers_))
    cc    = np.sort(np.squeeze(KMeans(n_clusters=3).fit(cont.reshape(-1, 1)).cluster_centers_))

    print('Cluster centering on SNR: ')
    print(csnr)
    print('Cluster centering on Blur_Index: ')
    print(cblur)
    print('Cluster centering on RMS Contrast: ')
    print(cc)
    return [csnr, cblur, cc]

def KMeans_class0(Blur_max, Blur_min,
                 cRMS_max,cRMS_min,
                  CSV_read, CSV_write):
    csv = pd.read_csv(CSV_read)
    File = csv['File'].tolist()
    SNR = csv['SNR'].tolist()
    BLUR = csv['Blur'].tolist()
    CONT_RMS = csv['Contrast'].tolist()
    filename = []
    snr1 = []
    blur1 = []
    CRMS1 = []
    n=0
    for snr,blur,CRMS,file in zip(SNR,BLUR,CONT_RMS,File):
        if (CRMS < cRMS_max or CRMS > cRMS_min):
            if ((snr < 1 ) or
                (blur < Blur_max or blur > Blur_min)):
                n+=1
                filename.append(file)
                snr1.append(snr)
                blur1.append(blur)
                CRMS1.append(CRMS)
    print('No. of Images: ')
    print(n)
    ''' Escribir CSV'''
    df1 = pd.DataFrame({'File': filename,
                        'SNR': snr1,
                        'Blur': blur1,
                        'Contrast': CRMS1})
    df1.to_csv(CSV_write+'_0.csv')


def KMeans_class1(Blur_max, Blur_min,
                  cRMS_max, cRMS_min,
                  CSV_read, CSV_write):
    csv = pd.read_csv(CSV_read)
    File = csv['File'].tolist()
    SNR = csv['SNR'].tolist()
    BLUR = csv['Blur'].tolist()
    CONT_RMS = csv['Contrast'].tolist()
    filename = []
    snr1 = []
    blur1 = []
    CRMS1 = []
    n=0
    for snr,blur,CRMS,file in zip(SNR,BLUR,CONT_RMS,File):
        if ((snr > 1) and
            (blur < Blur_max and blur > Blur_min) and
            (CRMS < cRMS_max and CRMS > cRMS_min)):
            n+=1
            filename.append(file)
            snr1.append(snr)
            blur1.append(blur)
            CRMS1.append(CRMS)
    print('No. of Images: ')
    print(n)
    ''' Escribir CSV'''
    df1 = pd.DataFrame({'File': filename,
                        'SNR': snr1,
                        'Blur': blur1,
                        'Contrast': CRMS1})
    df1.to_csv(CSV_write+'_1.csv')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-cr', help='CSV Read', required=True)
    parser.add_argument('-cw', help='CSV write', required=True)
    args = parser.parse_args()
    csv_path =  args.cr
    csv_write = args.cw

    '''***************************'''
    start = time() #Time Calculation
    KMeans_predict(csv_path, csv_write)
    '''get_cluster_points(csv_path)
    KMeans_class0( Blur_max= 10, Blur_min=64,
                   cRMS_max=0.12, cRMS_min=0.27,
                   CSV_read=csv_path, CSV_write=csv_write)
    KMeans_class1( Blur_max= 64, Blur_min=10,
                   cRMS_max=0.27, cRMS_min=0.12,
                   CSV_read=csv_path, CSV_write=csv_write)'''
    stop = time() #Time Calculation
    et = stop - start #Time Calculation
    print('[Elapsed Time]: '+ str(et) + ' s') #Print Time Calculation