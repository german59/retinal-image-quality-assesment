import cv2
import numpy as np
import random
from matplotlib import pyplot as plt
font                   = cv2.FONT_HERSHEY_SIMPLEX
bottomLeftCornerOfText = (50,50)
fontScale              = 0.7
fontColor              = (255,255,255)
lineType               = 2
image = cv2.imread('images/cap2/lena_gray.png', cv2.IMREAD_GRAYSCALE)
cv2.convertScaleAbs(image, image, 1.6, 0)

cv2.imwrite('images/cap2/lena_contrast_high.jpg', image)
plt.figure()
plt.hist(image.ravel(), bins=256, range=(0, 250), fc='w', ec='b')
plt.xlabel('Valor de pixel')
plt.ylabel('Cantidad de pixeles')
plt.savefig('images/cap2/hist_high')