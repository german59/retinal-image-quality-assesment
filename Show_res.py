import pandas as pd
from matplotlib import pyplot as plt
from time import time
from mpl_toolkits.mplot3d import Axes3D
import argparse



def show_4files(path1,path2,path3,path4,name):
    csv1 = pd.read_csv(path1)
    csv2 = pd.read_csv(path2)
    csv3 = pd.read_csv(path3)
    csv4 = pd.read_csv(path4)
    SNR1 = csv1['SNR'].to_numpy(float)
    blur1 = csv1['Blur'].to_numpy(float)
    cRMS1 = csv1['Contrast'].to_numpy(float)
    SNR2 = csv2['SNR'].to_numpy(float)
    blur2 = csv2['Blur'].to_numpy(float)
    cRMS2 = csv2['Contrast'].to_numpy(float)
    SNR3 = csv3['SNR'].to_numpy(float)
    blur3 = csv3['Blur'].to_numpy(float)
    cRMS3 = csv3['Contrast'].to_numpy(float)
    SNR4 = csv4['SNR'].to_numpy(float)
    blur4 = csv4['Blur'].to_numpy(float)
    cRMS4 = csv4['Contrast'].to_numpy(float)
    SNR = [SNR1, SNR2, SNR3, SNR4]
    blur = [blur1, blur2, blur3, blur4]
    cRMS = [cRMS1, cRMS2, cRMS3, cRMS4]
    #IND = [SNR, blur, cRMS]
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.scatter3D(SNR[0], blur[0], cRMS[0], c='r', marker='.', label='C0', s=1)
    ax.scatter3D(SNR[1], blur[1], cRMS[1], c='b', marker='.', label='C2', s=1)
    ax.scatter3D(SNR[2], blur[2], cRMS[2], c='k', marker='.', label='C3', s=1)
    ax.scatter3D(SNR[3], blur[3], cRMS[3], c='g', marker='.', label='C4', s=1)
    ax.legend()
    ax.set_xlabel('SNR')
    ax.set_ylabel('Blur')
    ax.set_zlabel('Contraste')
    plt.savefig(name + '_IN_FULL.png')
    plt.show()
def show_3files(path1,path2,path3,name):
    csv1 = pd.read_csv(path1)
    csv2 = pd.read_csv(path2)
    csv3 = pd.read_csv(path3)
    SNR1 = csv1['SNR'].to_numpy(float)
    blur1 = csv1['Blur'].to_numpy(float)
    cRMS1 = csv1['Contrast'].to_numpy(float)
    SNR2 = csv2['SNR'].to_numpy(float)
    blur2 = csv2['Blur'].to_numpy(float)
    cRMS2 = csv2['Contrast'].to_numpy(float)
    SNR3 = csv3['SNR'].to_numpy(float)
    blur3 = csv3['Blur'].to_numpy(float)
    cRMS3 = csv3['Contrast'].to_numpy(float)
    SNR = [SNR1, SNR2, SNR3]
    blur = [blur1, blur2, blur3]
    cRMS = [cRMS1, cRMS2, cRMS3]
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.scatter3D(SNR[0], blur[0], cRMS[0], c='r', marker='.', label='C0', s=1)
    ax.scatter3D(SNR[1], blur[1], cRMS[1], c='b', marker='.', label='C1', s=1)
    ax.scatter3D(SNR[2], blur[2], cRMS[2], c='k', marker='.', label='C2', s=1)
    ax.legend()
    ax.set_xlabel('SNR')
    ax.set_ylabel('Blur')
    ax.set_zlabel('Contraste')
    plt.savefig(name + '_IN_FULL.png')
    plt.show()
def show_2files(path1,path2, name):
    csv1 = pd.read_csv(path1)
    csv2 = pd.read_csv(path2)
    SNR1 = csv1['SNR'].to_numpy(float)
    blur1 = csv1['Blur'].to_numpy(float)
    cRMS1 = csv1['Contrast'].to_numpy(float)
    SNR2 = csv2['SNR'].to_numpy(float)
    blur2 = csv2['Blur'].to_numpy(float)
    cRMS2 = csv2['Contrast'].to_numpy(float)
    SNR = [SNR1, SNR2]
    blur = [blur1, blur2]
    cRMS = [cRMS1, cRMS2]
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.scatter3D(SNR1, blur1, cRMS1, c='r', marker='.', label=name+'_C0', s=1)
    ax.scatter3D(SNR2, blur2, cRMS2, c='b', marker='x', label=name+'_C1', s=1)
    ax.legend()
    ax.set_xlabel('SNR')
    ax.set_ylabel('Blur')
    ax.set_zlabel('Contraste')
    plt.savefig(name + '_IN_FULL.png')
    n=0
    for S, B, C in zip(SNR, blur, cRMS):
        plt.figure()
        plt.scatter(S, B, c='r', marker='.',label=name+'Class '+str(n))
        plt.xlabel('SNR')
        plt.ylabel('Blur')
        plt.legend(loc="best")
        plt.savefig(name+'Class_'+str(n) + '_1_IN_VS.png')
        plt.figure()
        plt.scatter(S, C, c='b', marker='.',label=name+'Class '+str(n))
        plt.xlabel('SNR')
        plt.ylabel('Contraste')
        plt.legend(loc="best")
        plt.savefig(name+'Class_'+str(n) + '_2_IN_VS.png')
        plt.figure()
        plt.scatter(C, B, c='g', marker='.',label=name+'Class '+str(n))
        plt.xlabel('Contraste')
        plt.ylabel('Blur')
        plt.legend(loc="best")
        plt.savefig(name+'Class_'+str(n) + '_3_IN_VS.png')
        plt.figure()
        plt.hist(S, color='r', bins=70, alpha=0.7, rwidth=0.85,label=name+'Class '+str(n))
        plt.xlabel('SNR')
        plt.ylabel('No. de Images')
        plt.legend(loc="best")
        plt.savefig(name+'Class_'+str(n) + '_1_N_IMG.png')
        plt.figure()
        plt.hist(B, color='b', bins=70, alpha=0.7, rwidth=0.85,label=name+'Class '+str(n))
        plt.xlabel('Blur')
        plt.ylabel('No. de Images')
        plt.legend(loc="best")
        plt.savefig(name+'Class_'+str(n) + '_2_N_IMG.png')
        plt.figure()
        plt.hist(C, color='g', bins=70, alpha=0.7, rwidth=0.85,label=name+'Class '+str(n))
        plt.xlabel('Contraste')
        plt.ylabel('No. de Images')
        plt.legend(loc="best")
        plt.savefig(name+'Class_'+str(n) + '_3_N_IMG.png')
        n+=1
def show_1files(path,name):
    csv1 = pd.read_csv(path)
    SNR = csv1['SNR'].tolist()
    blur = csv1['Blur'].tolist()
    cRMS = csv1['Contrast'].tolist()
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.scatter3D(SNR, blur, cRMS, c='g', marker='.', label=name, s=1)
    ax.legend()
    ax.set_xlabel('SNR')
    ax.set_ylabel('Blur')
    ax.set_zlabel('Contraste')
    plt.savefig(name + '_IN_FULL.png')
    plt.figure()
    plt.scatter(SNR, blur, c='r', marker='.', label=name)
    plt.xlabel('SNR')
    plt.ylabel('Blur')
    plt.savefig(name + '_1_IN_VS.png')
    plt.figure()
    plt.scatter(SNR, cRMS, c='b', marker='.', label=name)
    plt.xlabel('SNR')
    plt.ylabel('Contraste')
    plt.savefig(name + '_2_IN_VS.png')
    plt.figure()
    plt.scatter(cRMS, blur, c='g', marker='.', label=name)
    plt.xlabel('Contraste')
    plt.ylabel('Blur')
    plt.savefig(name + '_3_IN_VS.png')
    plt.figure()
    plt.hist(SNR, color='r', bins=70, alpha=0.7, rwidth=0.85, label=name)
    plt.xlabel('SNR')
    plt.ylabel('No. de Images')
    plt.savefig(name + '_1_N_IMG.png')
    plt.figure()
    plt.hist(blur, color='b', bins=70, alpha=0.7, rwidth=0.85, label=name)
    plt.xlabel('Blur')
    plt.ylabel('No. de Images')
    plt.savefig(name + '_2_N_IMG.png')
    plt.figure()
    plt.hist(cRMS, color='g', bins=70, alpha=0.7, rwidth=0.85, label=name)
    plt.xlabel('Contraste')
    plt.ylabel('No. de Images')
    plt.savefig(name + '3_N_IMG.png')

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', help='number of csv (1,2,3,4)', required=True)
    parser.add_argument('-c1', help='CSV1 file name', required=True)
    parser.add_argument('-c2', help='CSV2 file name', required=False)
    parser.add_argument('-c3', help='CSV3 file name', required=False)
    parser.add_argument('-c4', help='CSV4 file name', required=False)
    parser.add_argument('-nm', help='Name to save figures', required=True)
    args = parser.parse_args()
    n = args.n
    path1 = str(args.c1)
    path2 = str(args.c2)
    path3 = str(args.c3)
    path4 = str(args.c4)
    name = str(args.nm)

    start = time()  # Time Calculation
    if n == '1':
        show_1files(path1, name)  # Function 1 CSV file
    elif n == '2':
        show_2files(path1,path2,name) #Function 2 CSV files
    elif n == '3':
        show_3files(path1, path2, path3, name)  # Function 2 CSV files
    elif n == '4':
        show_4files(path1, path2, path3, path4, name)  # Function 2 CSV files
    else:
        print('Error -n debe ser 1 2 4')

    stop = time()  # Time Calculation
    et = stop - start  # Time Calculation
    print('[Elapsed Time]: ' + str(et) + ' s')  # Print Time Calculation

if __name__ == '__main__':
   main()