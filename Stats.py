import numpy as np
import pandas as pd
import argparse
from time import time
from scipy import stats
import seaborn as sns
from matplotlib import pyplot as plt
def statistics(data):
    print('Mean: '+ str(data.mean(axis=0)))
    print('Median: ' + str(np.median(data,0)))
    print('Std: ' + str(np.std(data,0)))
    print('Variance: ' + str(np.var(data, 0)))

    #print('Covariance: ' + str(np.cov(data)))
    print('Mode: ' + str(stats.mode(data)))
    plt.figure()
    sns.boxplot(data=data, color="blue", showfliers = False, width=0.4);
    plt.show()

    plt.figure()
    plt.hist(data[:,0], 100, histtype="stepfilled", alpha =0.8)
    plt.show()
    plt.figure()
    plt.hist(data[:, 1], 100, histtype="stepfilled", alpha=0.8)
    plt.show()
    plt.figure()
    plt.hist(data[:, 2], 100, histtype="stepfilled", alpha=0.8)
    plt.show()
def data_analisys(CSV_read):

    csv = pd.read_csv(CSV_read)
    print(csv.describe())
    FILE = csv['File']
    SNR = csv['SNR']
    BLUR = csv['Blur']
    CONT = csv['Contrast']
    file = FILE.to_list()
    snr = SNR.to_numpy(float)
    blur = BLUR.to_numpy(float)
    cont = CONT.to_numpy(float)
    x = np.array([snr, blur, cont])
    data = x.transpose((1,0))
    statistics(data)
    print(data.shape)
    plt.figure()
    sns.heatmap(csv.corr())
    plt.show()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-cr', help='CSV Read', required=True)
    args = parser.parse_args()
    csv_path = args.cr
    '''***************************'''
    start = time() #Time Calculation
    data_analisys(csv_path)
    stop = time() #Time Calculation
    et = stop - start #Time Calculation
    print('[Elapsed Time]: '+ str(et) + ' s') #Print Time Calculation