import pandas as pd
import os
import shutil
import multiprocessing as mp
import argparse
from time import time

parser = argparse.ArgumentParser()
parser.add_argument('-s', help='Source read image path', required=True)
parser.add_argument('-d', help='Destiny write image path', required=True)
parser.add_argument('-c', help='CSV read filenames', required=True)
args = parser.parse_args()
Read_path = args.s
Write_path = args.d
Read_CSV = args.c

def get_filename_from_csv(path, Read_CSV):
    csv = pd.read_csv(Read_CSV, sep = ',', quotechar="'", dtype={"File": str})
    File = csv['File'].tolist()
    SNR = csv['Predict'].tolist()
    filename = []
    for noise, file in zip(SNR, File):
        try:
            filename.append(os.path.join(path, file))
        except:
            continue
    return filename

def copy_file(file):
    path = args.d
    try:
        shutil.copy(os.path.join(file), path)
    except:
        pass

def main():
    procs = mp.cpu_count()
    pool = mp.Pool(processes=procs)
    start = time()
    file = get_filename_from_csv(Read_path, Read_CSV)
    pool.map(copy_file, file)
    pool.close()
    pool.join()
    stop = time()
    et = stop - start
    print('[Elapsed Time]: ' + str(et) + ' seg')
if __name__ == '__main__':
    main()