'''
Barcelona Supercomputing Center (BSC-CNS)
Abraham Sanchez Perez
Dr. Eduardo Ulises Moya Sanchez
06/2018

This code provides a log file that contains information about the 
quality measurement of images such as SNR, blur rate and contrast. 
In addition, create the graph of these attributes.
'''

import cv2 
import numpy as np
import pandas
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join
import timeit
from math import fmod
import math
import platform
import os
import fnmatch
import logging
import sys
import time
from glob import glob
from multiprocessing import Process, Pool
import multiprocessing
from skimage.measure import shannon_entropy
import argparse

class ImageMeasure:

  '''
  Constructor
  '''
  def __init__(self, path, log_name=None):
    # Set the lof file name and the source path.
    self.__img_path = path
    self.__log_name = log_name + '.log'

  '''
  Create the log file.
  '''
  def init_logger(self, log_name, log_file):
    log = logging.getLogger(log_name)
    formatter = logging.Formatter('%(message)s')
    fhok = logging.FileHandler(log_file, mode='w')
    fhok.setFormatter(formatter)
    ch = logging.StreamHandler(sys.stdout)
    log.setLevel(logging.INFO)
    log.addHandler(fhok)
    log.addHandler(ch)
    return log

  '''
  Get a list of image file names.
  '''
  def get_image_file_name_list(self):
    return  [y for x in os.walk(self.__img_path) for y in glob(os.path.join(x[0], '*.jpg'))]

  '''
  Save measure information in the log file.
  '''
  def log_results(self, results):
    gsnr = results[:,1].astype(np.float)
    gcontrast = results[:,2].astype(np.float)
    gblur = results[:,3].astype(np.float)
    snr_mean = np.mean(results[:,1].astype(np.float))
    snr_var = np.var(results[:,1].astype(np.float))
    cont_mean = np.mean(results[:,2].astype(np.float))
    cont_var = np.var(results[:,2].astype(np.float))
    blur_mean = np.mean(results[:,3].astype(np.float))
    blur_var = np.var(results[:,3].astype(np.float))
    log = self.init_logger('ok', self.__log_name)
    for n in range(0, len(results)):
      img_name = results[n][0]
      snr = results[n][1].astype(np.float)
      cont = results[n][2].astype(np.float)
      blur = results[n][3].astype(np.float)
      log.info("{},{},{},{}".format(img_name,snr, cont, blur))
    log.info("{},{},{},{}".format('mean',snr_mean,cont_mean,blur_mean))
    log.info("{},{},{},{}".format('var',snr_var,cont_var, blur_var))
    # Plot measure information
    self.plot_quality(gsnr, gcontrast, gblur)

  '''
  Create graphs of SNR, blur index and contrast
  '''
  def plot_quality(self, snr, contrast, blur):
    plt.clf()
    plt.hist(snr, 200,color='b')
    plt.gca().set_title('SNR')
    plt.savefig(self.__log_name+'snr.jpg')

    plt.clf()
    plt.hist(contrast, 200, color='b')
    plt.gca().set_title('Contrast')
    plt.savefig(self.__log_name+'contrast.jpg')

    plt.clf()
    plt.hist(blur, 200, [0, 700], color='b')
    plt.gca().set_title('Blur index')
    plt.savefig(self.__log_name+'blur.jpg')

  '''
  Get SNR, blur index and contract value about a specific image.
  '''
  def get_attributes(self, image):
    img = cv2.imread(image)
    imgray = cv2.imread(image, 0)
    # Get SNR
    mean,std = cv2.meanStdDev(imgray)
    snr=(mean/std)
    # Get blur index 
    blur_index=cv2.Laplacian(imgray, cv2.CV_64F).var()
    # Get contrast
    hist,bins = np.histogram(img.ravel(),256,[0,256])
    hist = np.trim_zeros(hist)
    mid = math.ceil(len(hist)/2)
    hminp = hist[:mid]
    hmaxp = hist[mid:]
    lmax = np.mean(hmaxp)
    lmin = np.mean(hminp)
    contrast = abs((lmax - lmin) / (lmax + lmin))
    return [image, snr[0][0], contrast, blur_index]

  '''
  Get quality measure of the images.
  '''
  def process_images(self, images):
    current_total_time = time.time()
    with Pool(multiprocessing.cpu_count()) as pool:
      results = np.array(pool.map(self.get_attributes, images))
    diff_total_time = (time.time() - current_total_time)
    print("Total: ", diff_total_time)
    if len(results) > 0:
      self.log_results(results)

'''
Main function.
'''
def main():
    py_v = platform.python_version()
    print ("\nRunning on Python version ", py_v)
    description = 'Measurer Quality Image.'

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-s', help='Source image path',required=True)
    parser.add_argument('-f', help='Log file name', required=True)
    args = parser.parse_args()
    log = args.f
    path = args.s
    measure = ImageMeasure(path, log)
    images = measure.get_image_file_name_list()
    measure.process_images(images)

if __name__ == '__main__':
    main()
