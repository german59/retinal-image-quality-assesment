from PIL import Image, ImageStat
import numpy as np
import math
import imquality.brisque as brisque
import cv2
from skimage import io
def snr(image):
    gray = np.array(Image.open(image).convert('L')) / 255.
    mean, std = np.mean(gray), np.std(gray)
    snr = mean / std
    return snr

def michelson(image):
    gray = np.array(Image.open(image).convert('L'))
    hist, _ = np.histogram(gray.ravel(), 256, [0, 255])
    hist = np.trim_zeros(hist)
    mid = math.ceil(len(hist) / 2)
    hminp = hist[:mid]
    hmaxp = hist[mid:]
    lmax = np.mean(hmaxp)
    lmin = np.mean(hminp)
    contrast = np.abs((lmax - lmin) / (lmax + lmin))
    return contrast

def laplacian(image):
    bgr = cv2.imread(image, cv2.IMREAD_COLOR)
    blur = cv2.Laplacian(bgr,cv2.CV_64F).var()
    return blur

def bris(image):
    rgb = Image.open(image)
    return brisque.score(rgb)

def sharpnes():
    pass

def brightness(image):
   im = Image.open(image)
   stat = ImageStat.Stat(im)
   r,g,b = stat.mean
   return math.sqrt(0.241*(r**2) + 0.691*(g**2) + 0.068*(b**2))