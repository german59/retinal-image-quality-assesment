import cv2
import os
from time import time
import multiprocessing as mp
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-s', help='Source image path', required=True)
parser.add_argument('-d', help='Destiny image path', required=True)
parser.add_argument('-k', help='Kernel Value', required=True)
args = parser.parse_args()
source = args.s
dest = args.d
kernel = int(args.k)
def gen_blur(filename):
    save_path = dest
    image = cv2.imread(filename, cv2.IMREAD_COLOR)
    kernel1 = (kernel,kernel)
    gauss = cv2.GaussianBlur(image,kernel1,cv2.BORDER_DEFAULT)
    file = os.path.split(filename)
    cv2.imwrite(os.path.join(save_path, file[1]), gauss)

if __name__ == '__main__':
    read_path = source
    mgr = mp.Manager()#Multiprocessing
    procs = mp.cpu_count()#Multiprocessing
    file = []
    N = 0
    for filename in os.listdir(read_path):
            file.append(os.path.join(read_path, filename))
    pool = mp.Pool(processes=procs)#Multiprocessing
    start = time()
    pool.map(gen_blur, file)  # ***** Function *****
    stop = time()
    print('[Elapsed Time]: '+ str(stop-start) + ' s')
    pool.close()  # multiprocessing
    pool.join()  # multiprocessing