import cv2
import os
import multiprocessing as mp
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-s', help='Source image path', required=True)
parser.add_argument('-d', help='Destiny image path', required=True)
args = parser.parse_args()
source = args.s
dest = args.d
def clahe(filename):
    save_path = dest
    image = cv2.imread(filename, cv2.IMREAD_COLOR)
    lab = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    lab_planes = cv2.split(lab)
    clahe = cv2.createCLAHE(clipLimit=1, tileGridSize=(8, 8))
    lab_planes[0] = clahe.apply(lab_planes[0])
    lab = cv2.merge(lab_planes)
    new_image = cv2.cvtColor(lab, cv2.COLOR_LAB2BGR)
    pre = os.path.split(filename)
    cv2.imwrite(os.path.join(save_path,pre[1]), new_image)
    print(os.path.join(save_path,pre[1]))

if __name__ == '__main__':
    read_path = source
    mgr = mp.Manager()#Multiprocessing
    procs = mp.cpu_count()#Multiprocessing
    file = []
    for filename in sorted(os.listdir(os.path.join(read_path))):
        file.append(os.path.join(read_path, filename))
    pool = mp.Pool(processes=procs) #Multiprocessing
    pool.map(clahe, file)  # ***** Function *****
    pool.close()  # multiprocessing
    pool.join()  # multiprocessing