import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import load_model
from PIL import Image
from PIL import ImageFilter
from scipy.ndimage import label
import multiprocessing
from time import time
import argparse
import os
import cv2
def remove_remaining(prediction):
    '''
    Remove the remaining groups of prediction pixels.
    :param prediction: Pillow Image, prediction (segmentation) of the model.
    :return: Pillow Image.
    '''
    prediction = np.array(prediction)
    labels, features = label(prediction)
    h, w = prediction.shape
    groups = np.unique(labels)

    # Since label method converts the values of the matrix, we don't know which
    # ones are the black pixels so it will help to identify them by pixel counter.
    # Most of the times zeros of labels means zeros of the prediction but it
    # could change so we must ensure.
    zeros_counter = prediction[(prediction == 0)].size
    zeros_id = 0

    remainings = []
    for group in groups:
        count = labels[(labels == group)].size
        if count == zeros_counter:
            zeros_id = group
        percentage = count/(h*w)
        if percentage < 0.30:
            remainings.append(group)

    for remaining_group in remainings:
        labels = np.where(labels==remaining_group, zeros_id, labels)

    # Reconvert to binary format
    labels = np.where(labels != zeros_id, 255, labels)
    labels = np.where(labels == zeros_id, 0, labels)
    return Image.fromarray(labels.astype(np.uint8))


def add_zerosboxes(img):
    """
    Add zero boxes to the retina image to adjust the size to nxn resolution.

    :param img: numpyarray, The image.
    :return: Numpy array (image) with zeros boxes.
    """
    h, w, c = img.shape
    axis = 0
    b = abs(int(h - w))
    b1 = int(b / 2)
    b2 = b - b1
    if h > w:
        axis = 1
        z1 = np.zeros((h, b1, c))
        z2 = np.zeros((h, b2, c))
    elif w > h:
        z1 = np.zeros((b1, w, c))
        z2 = np.zeros((b2, w, c))
    else:
        return img
    newimg = np.append(img, z1, axis=axis)
    newimg = np.append(z2, newimg, axis=axis)
    return newimg


def crop_retina(img_file, model_name):
    """
    Perform the prediction of the retina's mask.

    :param src_path: str, Retina images directory name.
    :param dst_path: str, Destination directory name where the crop will be stored.
    :param model_name: st, Name of the retina crop model.
    :return: Nothing.
    """
    tf.autograph.set_verbosity(0)
    workers = multiprocessing.cpu_count()
    #create_directory(dst_path)

    model = load_model(model_name)
    _, h, w, _ = model.input.shape
    pred_size = (h, w)
    img = Image.open(img_file).convert('RGB')
    h, w = img.size
    gray = img.convert('L')
    resimg = gray.resize(pred_size)
    resimg = np.array(resimg)
    resimg = resimg.astype(np.float32)
    resimg = resimg / 255.
    resimg = np.expand_dims(resimg, axis=0)
    resimg = np.expand_dims(resimg, axis=-1)
    pred = model.predict(resimg, verbose=1, workers=workers, use_multiprocessing=True)
    pred = np.where(pred > .5, 255, 0).astype(np.uint8)
    pred = np.reshape(pred, pred_size)
    pred = Image.fromarray(pred)
    pred = pred.filter(ImageFilter.MinFilter(11))
    pred = remove_remaining(pred)
    pred = pred.resize((h, w))
    newimg = np.array(pred)
    pos = np.where(newimg)
    if is_retina_mask_empty(pos):
        print('Invalid image!')
    xmin = np.min(pos[1])
    xmax = np.max(pos[1])
    ymin = np.min(pos[0])
    ymax = np.max(pos[0])
    img = np.array(img)
    crop = img[ymin:ymax, xmin:xmax]
    crop = add_zerosboxes(crop).astype(np.uint8)
    crop = Image.fromarray(crop)
    return crop, pred


def is_retina_mask_empty(retina_mask):
    for mask in retina_mask:
        if mask.size == 0:
            return True

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', help='Source image path', required=True)
    parser.add_argument('-d', help='Destine image path', required=True)
    parser.add_argument('-m', help='Model hdf5 file', required=True)
    args = parser.parse_args()
    start = time()
    file = []
    read_path = args.s
    write_path = args.d
    for filename in os.listdir(read_path):
        if filename.endswith(('jpg', 'jpeg', 'png', 'tif', 'PNG', 'JPG','JPEG')):
            file.append(os.path.join(read_path, filename))
    for img_file in file:
        try:
            crop, pred = crop_retina(img_file, args.m)
            crop.save(write_path + img_file)
        except:
            continue
        #pred.save(write_path + '_pred_'+img_file)
    stop = time()
    et = stop - start
    print('[Elapsed time]: ' + str(et) + ' s')

if __name__ == '__main__':
    main()