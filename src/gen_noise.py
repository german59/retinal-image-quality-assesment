import cv2
import os
import multiprocessing as mp
import numpy as np
import  argparse

parser = argparse.ArgumentParser()
parser.add_argument('-s', help='Source image path', required=True)
parser.add_argument('-d', help='Destiny image path', required=True)
args = parser.parse_args()
source = args.s
dest = args.d
def gen_noise(filename):
    save_path = dest
    im_read = cv2.imread(filename, cv2.IMREAD_COLOR)
    im_read = cv2.cvtColor(im_read,cv2.COLOR_BGR2HSV)
    H, S, V = cv2.split(im_read)
    row, col, ch = im_read.shape
    # ****************** addNoise ******************
    mean = 0
    var = 0.01
    sigma = var ** 0.5
    gauss = np.random.normal(mean, sigma, (row, col))
    gauss = gauss.reshape(row, col).astype('uint8')
    # Add the Gaussian noise to the image
    noisy_image = cv2.add(V, gauss)
    im_out = cv2.merge((H,S,noisy_image))
    im_out = cv2.cvtColor(im_out, cv2.COLOR_HSV2BGR)
    file = os.path.split(filename)
    cv2.imwrite(os.path.join(save_path, file[1]), im_out)

if __name__ == '__main__':
    read_path = args.s
    mgr = mp.Manager()      #Multiprocessing
    procs = mp.cpu_count()  #Multiprocessing
    file = []
    for filename in os.listdir(read_path):
        file.append(os.path.join(read_path, filename))
        #print(file)
    pool = mp.Pool(processes=procs) #Multiprocessing
    # >>>>>>>>>***** Function *****<<<<<<<<<<<<
    pool.map(gen_noise, file)
    #******************************************
    pool.close()  #Multiprocessing
    pool.join()  #Multiprocessing