import cv2
import os
import multiprocessing as mp
from time import time
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-s', help='Source image path', required=True)
parser.add_argument('-d', help='Destination image path', required=True)
parser.add_argument('-v', help='Contrast Value', required=True)
args = parser.parse_args()
source = args.s
dest = args.d
value = args.v
def gen_contrast(filename):
    save_path = dest
    image = cv2.imread(filename, cv2.IMREAD_COLOR)
    rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    cv2.convertScaleAbs(rgb, rgb, value, 0)
    file = os.path.split(filename)
    new_image = cv2.cvtColor(rgb, cv2.COLOR_RGB2BGR)
    cv2.imwrite(os.path.join(save_path, file[1]), new_image)

if __name__ == '__main__':
    read_path = source
    mgr = mp.Manager()#Multiprocessing
    procs = mp.cpu_count()#Multiprocessing
    file = []
    N = 0
    for filename in os.listdir(read_path):
        if 1088 <= N:
            file.append(os.path.join(read_path, filename))
        N += 1
    pool = mp.Pool(processes=procs)#Multiprocessing
    start = time()  # Time Calculation
    pool.map(gen_contrast, file)  # ***** Function *****
    pool.close()  # multiprocessing
    pool.join()  # multiprocessing
    stop = time() #Time Calculation
    et = stop - start #Time Calculation
    print('[Elapsed Time]: '+ str(et) + ' s') #Print Time Calculation